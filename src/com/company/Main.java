package com.company;

import com.company.crawler.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;


public class Main extends Application{

    CustomTabPane layout = new CustomTabPane();
    Thread crawlerThread;
    public static void main(String[] args) throws NoInputExpection, IOException, InterruptedException {
        launch(args);

    }

    public void startScene(Stage primaryStage)
    {

        primaryStage.setTitle("Crawler");

        layout.addComponents(primaryStage,crawlerThread);
        Scene scene = new Scene(layout,800,700);
        primaryStage.setScene(scene);
        primaryStage.show();

    }



    @Override
    public void start(Stage primaryStage) throws Exception {


        startScene(primaryStage);

        crawlerThread = new CrawlerTread(layout.customTableView,layout.logTableView,layout.customBarChart);
        crawlerThread.start();








    }
}
