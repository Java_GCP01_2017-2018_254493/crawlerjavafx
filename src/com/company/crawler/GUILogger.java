package com.company.crawler;

import com.company.CustomBarChart;
import com.company.CustomTabPane;
import com.company.CustomTableView;

import java.io.IOException;

/**
 * Created by Bartek on 2017-03-24.
 */
public class GUILogger implements Logger {


    private CustomTableView _table;
    private CustomTableView _tableLog;
    private CustomBarChart _barChart;

   public GUILogger(CustomTableView table,CustomTableView logTable,CustomBarChart barChart)
   {
     _table=table;
       _tableLog=logTable;
       _barChart=barChart;
   }

    @Override
    public void log(String status, Student student)  {
        _barChart.getMark(student);
        if (status == "ADDED")
        {
            _table.getStudents(student);
            _tableLog.getLogs(student,status);


        }
        if(status == "REMOVED") {
             _table.deleteStudent(student);
            _tableLog.getLogs(student,status);
        }

        if(status == "MODIFY") {
            _tableLog.getLogs(student,status);
            try {
                _table.modifyStudend();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
