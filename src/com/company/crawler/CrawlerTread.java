package com.company.crawler;

import com.company.CustomBarChart;
import com.company.CustomTableView;

import java.io.IOException;

/**
 * Created by Bartek on 2017-03-24.
 */
public class CrawlerTread extends Thread {


    CustomTableView _tableStudents;
    CustomTableView _tableLog;
    private CustomBarChart _barChart;
  public CrawlerTread(CustomTableView table,CustomTableView logTable,CustomBarChart barChart)
  {
      _tableStudents = table;
      _tableLog=logTable;
      _barChart=barChart;
  }

    public void run() {
        Crawler proba = new Crawler();

        final Logger[] loggers = new Logger[]
                {
                        new GUILogger(_tableStudents,_tableLog,_barChart),
                        new ConsolLogger()
                };
        proba.addIterationStartedListener((i)->{System.out.println("Numer " +i);});
        proba.addToAddStudentsListnerList((student)->{
            String status = proba.isAdd(student);
            for( Logger el : loggers )
                el.log(status, student );

        });
        proba.addToDeleteStudentsListnerList((student)->{
            String status = proba.isRemove(student);
            for( Logger el : loggers )
                el.log(status, student );
        });
        proba.addToModifyStudentsListnerList((student)->{
            String status = proba.isModify(student);
            for( Logger el : loggers )
                el.log(status, student );
        });
        try {
            proba.Run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
